const path = require('path')
const fs = require('fs')

const getFile = (fileName) => {
    const getPath = (filename) => path.join(__dirname, '..', 'data', filename)
    return new Promise((resolve, reject) => {
        fs.readFile(getPath(fileName), 'utf-8', function (err, data) {
            if (err) reject('Fail to read messages')
            else resolve(JSON.parse(data))
        })
    })
}

const updateFile = (fileName, dataObj) => {
    const getPath = (filename) => path.join(__dirname, '..', '/data', filename)
    const jsonData = JSON.stringify(dataObj)
    return new Promise((resolve, reject) => {
        fs.writeFile(getPath(fileName), jsonData, function (err, data) {
            if (err) reject('Fail to update messages')
            else resolve(data)
        })
    })
}

module.exports = {
    getFile,
    updateFile,
}
