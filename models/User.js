const { Schema, model, Types } = require('mongoose')

const shema = new Schema({
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    name: { type: String, required: true },
    role: { type: String, required: false }
})

module.exports = model('User', shema)
