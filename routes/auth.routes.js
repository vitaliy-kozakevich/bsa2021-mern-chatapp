const { Router } = require('express')
const bcrypt = require('bcryptjs')
const config = require('config')
const { check, validationResult } = require('express-validator')
const jwt = require('jsonwebtoken')
const User = require('../models/User')
const router = Router()

// /api/auth/register
router.post(
    '/register',
    [
        check('email', 'Incorrect email').isEmail(),
        check('password', 'Minimal password length 6 symbols').isLength({ min: 4 }),
        check('name', 'Minimal name length 3 symbols').isLength({ min: 3 }),
    ],
    async (req, res) => {
        try {
            const errors = validationResult(req)

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: 'Incorrect registration data',
                })
            }

            const { email, password, name, role } = req.body

            const candidate = await User.findOne({ email })

            if (candidate) {
                return res.status(400).json({ message: 'Such user exists' })
            }

            const hashedPassword = await bcrypt.hash(password, 12)
            const user = new User({
                email,
                password: hashedPassword,
                name,
                role: role ? role : 'user',
            })

            await user.save()

            res.status(201).json({ message: 'User has been created' })
        } catch (e) {
            res.status(500).json({ message: 'Something went wrong. Try again later' })
        }
    }
)

// /api/auth/login
router.post(
    '/login',
    [check('email', 'Type correct email').normalizeEmail().isEmail(), check('password', 'Type password').exists()],
    async (req, res) => {
        try {
            const errors = validationResult(req)

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: 'Incorrect login data',
                })
            }

            const { email, password } = req.body

            const user = await User.findOne({ email })

            if (!user) {
                return res.status(400).json({ message: 'User has not been found!' })
            }

            const isMatch = await bcrypt.compare(password, user.password)

            if (!isMatch) {
                return res.status(400).json({ message: 'Incorrect password. Please try again!' })
            }

            const token = jwt.sign({ userId: user.id }, config.get('jwtSecret'), {
                expiresIn: '1h',
            })

            res.json({ token, userId: user.id, name: user.name, role: user.role })
        } catch (e) {
            res.status(500).json({ message: 'Something went wrong. Try again later' })
        }
    }
)

module.exports = router
