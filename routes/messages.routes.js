const { Router } = require('express')
const config = require('config')
const { check, validationResult } = require('express-validator')
const { getFile, updateFile } = require('../helpers/processFile')
const router = Router()

router.get('/', async (req, res) => {
    try {
        const messages = await getFile('messages.json')
        if (!messages) {
            throw new Error('Something went wrong. Try again later')
        }

        res.json({ data: messages })
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
})

router.post(
    '/',
    check('userId', 'User id is not exists!').notEmpty().isString(),
    check('user', 'User name is not exists!').notEmpty().isString(),
    check('text', 'Message is not exists!').notEmpty().isString(),
    async (req, res) => {
        try {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: 'Incorrect message data',
                })
            }

            const newMessage = {
                id: new Date().getTime() - config.get('humanReadableIdOffset'),
                createdAt: new Date().toISOString(),
                editedAt: '',
            }
            const knownProperties = ['userId', 'user', 'text']
            knownProperties.forEach((prop) => {
                if (req.body.hasOwnProperty(prop)) newMessage[prop] = req.body[prop]
            })

            const messages = await getFile('messages.json')
            if (!messages) {
                throw new Error('Something went wrong. Try again later')
            }
            messages.push(newMessage)

            await updateFile('messages.json', messages)

            res.json({ data: newMessage })
        } catch (error) {
            res.status(500).json({ message: error.message })
        }
    }
)

router.put('/:id', check('text', 'Message is not exists!').notEmpty().isString(), async (req, res) => {
    console.log(req.body);
    try {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            return res.status(400).json({
                errors: errors.array(),
                message: 'Incorrect message data',
            })
        }

        const {
            params: { id },
        } = req
        const { text } = req.body
        const messages = await getFile('messages.json')
        if (!messages) {
            throw new Error('Something went wrong. Try again later')
        }

        const messageIdx = messages.findIndex((m) => m.id === +id)
        if (!messageIdx === -1) {
            throw new Error(`Message with id: ${id} is not exist`)
        }

        const message = messages[messageIdx]
        Object.assign(message, {
            text,
            editedAt: new Date().toISOString(),
        })
        messages[messageIdx] = message

        await updateFile('messages.json', messages)

        res.json({ id, text: message.text })
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
})

router.delete(
    '/:id',
    check('email', 'Incorrect email').isEmail(),
    check('name', 'Minimal name length 3 symbols').isLength({ min: 3 }),
    async (req, res) => {
        try {
            const {
                params: { id },
            } = req

            const messages = await getFile('messages.json')
            if (!messages) {
                throw new Error('Something went wrong. Try again later')
            }

            const messageIdx = messages.findIndex((m) => {
                return m.id === +id
            })
            if (messageIdx === -1) {
                throw new Error(`Message with id: ${id} is not exist`)
            }

            const newMessages = messages.filter((m) => m.id !== +id)

            console.log(id, messageIdx);

            await updateFile('messages.json', newMessages)

            res.json({ id })
        } catch (error) {
            res.status(500).json({ message: error.message })
        }
    }
)

module.exports = router
