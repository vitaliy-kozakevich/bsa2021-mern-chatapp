const { Router } = require('express')
const bcrypt = require('bcryptjs')
const { check, validationResult } = require('express-validator')
const User = require('../models/User')
const router = Router()

router.get('/', async (req, res) => {
    try {
        const users = await User.find()

        if (!users.length) {
            return res.status(400).json({ message: 'Users has not been found!' })
        }

        res.json({ users })
    } catch (error) {
        res.status(500).json({ message: 'Something went wrong. Try again later' })
    }
})

router.post(
    '/',
    check('email', 'Incorrect email').isEmail(),
    check('name', 'Minimal name length 3 symbols').isLength({ min: 3 }),
    check('password', 'Minimal password length 6 symbols').isLength({ min: 6 }),
    async (req, res) => {
        try {
            const errors = validationResult(req)

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: 'Incorrect user data',
                })
            }
            
            const candidate = await User.findOne({ email })
            const { email, password, name } = req.body

            if (candidate) {
                return res.status(400).json({ message: 'Such user exists' })
            }

            const hashedPassword = await bcrypt.hash(password, 12)
            const user = new User({ email, password: hashedPassword, name })
            await user.save()

            res.status(201).json({ message: 'User has been created' })
        } catch (error) {
            res.status(500).json({ message: 'Something went wrong. Try again later' })
        }
    }
)

router.put(
    '/:id',
    check('email', 'Incorrect email').isEmail(),
    check('name', 'Minimal name length 3 symbols').isLength({ min: 3 }),
    check('password', 'Minimal password length 6 symbols').isLength({ min: 6 }),
    async (req, res) => {
        try {
            const errors = validationResult(req)

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: 'Incorrect user data',
                })
            }
            
            const { params: { id } } = req
            const { email, password, name } = req.body
            
            const candidate = await User.findOne({ id })

            if (!candidate) {
                return res.status(400).json({ message: `User with id: ${id} is not exist!` })
            }

            const hashedPassword = await bcrypt.hash(password, 12)
            const user = new User({ email, password: hashedPassword, name })

            await user.save()

            res.status(200).json({ message: `User with id: ${id} has been updated` })
        } catch (error) {
            res.status(500).json({ message: 'Something went wrong. Try again later' })
        }
    }
)

router.delete(
    '/:id',
    async (req, res) => {
        try {
            const { params: { id } } = req

            const candidate = await User.findById(id)

            if (!candidate) {
                return res.status(400).json({ message: `User with id: ${id} is not exist!` })
            }

            await User.findByIdAndRemove(id)

            const users = await User.find()

            res.status(200).json({ id, message: `User with id: ${id} has been deleted` })
        } catch (error) {
            res.status(500).json({ message: 'Something went wrong. Try again later' })
        }
    }
)

module.exports = router
