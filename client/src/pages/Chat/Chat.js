import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { addMessage } from '../../redux/actions/messages'
import { ChatContext } from '../../components/context/Chat.context'
import { useMessage } from '../../components/hooks/message.hook'
import ChatHeader from '../../components/ChatHeader/ChatHeader'
import InputGroup from '../../components/InputGroup/InputGroup'
import MessageList from '../../components/MessageList/MessageList'

function Chat({ loading, error, user, sendMessage }) {
    const [inputValue, setInputValue] = useState('')
    const message = useMessage()

    useEffect(() => {
        message(error)
    }, [error, message])

    useEffect(() => {
        window.M.updateTextFields()
    }, [])

    const inputValueHandler = (e) => {
        setInputValue(e.target.value)
    }

    const sendMessageHandler = () => {
        if (!inputValue) {
            message('Type your message first')
            return
        }
        sendMessage(inputValue, user)
        setInputValue('')
    }

    if (loading) {
        return (
            <div style={{ textAlign: 'center', marginTop: '20%' }}>
                <img src={'../../assets/spinner.svg'} alt="spinner" />
            </div>
        )
    }

    return (
        <ChatContext.Provider value={{ inputValue, inputValueHandler, sendMessageHandler }}>
            <ChatHeader />
            <MessageList />
            <InputGroup />
        </ChatContext.Provider>
    )
}

function mapStateToProps(state) {
    return {
        messages: state.messages.messages,
        loading: state.chat.loading,
        error: state.chat.error,
        user: {
            userId: state.auth.userId,
            user: state.auth.name,
        },
    }
}

function mapDispatchToProps(dispatch) {
    return {
        sendMessage: (text, user) => dispatch(addMessage(text, user)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat)
