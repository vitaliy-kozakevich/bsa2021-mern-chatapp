import React, { useEffect, useState } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import Input from '../../components/UI/Input/Input'
import Button from '../../components/UI/Button/Button'
import './EditMessage.scss'
import { editMessageText } from '../../redux/actions/messages'
import { useMessage } from '../../components/hooks/message.hook'

function EditMessage({ history, match, messages, editMessage }) {
    const [inputValue, setInputValue] = useState('')
    const openChatPage = () => {
        history.push('/chat')
    }
    const onChange = ({ target: { value } }) => {
        setInputValue(value)
    }
    const messageId = +match.params.id
    const message = useMessage()

    useEffect(() => {
        const { text } = messages.find((m) => m.id === messageId)
        setInputValue(text)
    }, [])

    return (
        <div className="container">
            <div className="row">
                <div className="col s8 offset-s2">
                    <h3 className="center-align">Edit Message</h3>
                    <Input placeholder={'Type message'} value={inputValue} onChange={onChange} />
                    <div className="EditMessage__btns">
                        <div className="EditMessage__btn">
                            <Button clickHandler={openChatPage} name={'Cancel'} cls={['error', 'btn']} />
                        </div>
                        <div className="EditMessage__btn">
                            <Button
                                clickHandler={() => {
                                    if (!inputValue) message('Type your message first')
                                    editMessage(messageId, inputValue)
                                    openChatPage()
                                }}
                                name={'Edit'}
                                cls={['success', 'btn']}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

function mapStateToProps(state) {
    return {
        messages: state.messages.messages,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        editMessage: (id, text) => dispatch(editMessageText(id, text)),
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EditMessage))