import React, { useState, useEffect, createRef } from 'react'
import { connect } from 'react-redux'
import { setUserData } from '../../redux/actions/auth'
import RequestService from '../../services/request.service'
import AuthService from '../../services/auth.service'
import Input from '../../components/UI/Input/Input'
import './Auth.scss'
import { useMessage } from '../../components/hooks/message.hook'

const formConstrolsDefault = {
    name: {
        value: '',
        type: 'text',
        name: 'name',
        label: 'Name',
        placeholder: 'Type name',
        errorMessage: 'Type correct name',
    },
    email: {
        value: '',
        type: 'email',
        name: 'email',
        label: 'Email',
        placeholder: 'Type email',
        errorMessage: 'Type correct email',
    },
    password: {
        value: '',
        type: 'password',
        name: 'password',
        label: 'Password',
        placeholder: 'Type password',
        errorMessage: 'Type correct password',
    },
}

function Auth({ setUserData }) {
    const [name, setName] = useState(formConstrolsDefault.name)
    const [email, setEmail] = useState(formConstrolsDefault.email)
    const [password, setPassword] = useState(formConstrolsDefault.password)

    const requestService = new RequestService()
    const authService = new AuthService()
    const message = useMessage()
    const tabsRef = createRef()

    const loginHandler = async () => {
        try {
            const { token, userId, role, name } = await requestService.login('/auth/login', 'POST', {
                email: email.value,
                password: password.value,
            })
            authService.login({ token, name, role, userId })
            setUserData({ userId, role, name, token })
        } catch (error) {
            message(error.message)
        }
    }

    const registerHandler = async () => {
        try {
            const response = await requestService.register('/auth/register', 'POST', {
                name: name.value,
                email: email.value,
                password: password.value,
            })
            if (response.message) message(response.message)
        } catch (error) {
            message(error.message)
        }
    }

    const submitHandler = (event) => {
        event.preventDefault()
    }

    const onChangeHandler = (event, controlName) => {
        const form = { name, email, password }
        const control = { ...form[controlName] }

        control.value = event.target.value

        if (controlName === 'name') setName(control)
        if (controlName === 'email') setEmail(control)
        if (controlName === 'password') setPassword(control)
    }

    const renderInputs = (login) => {
        const formControls = { name, email, password }
        let formControlsKeys = Object.keys(formControls)

        if (login) {
            formControlsKeys = formControlsKeys.slice(1)
        }

        return formControlsKeys.map((controlName, index) => {
            const control = formControls[controlName]
            return (
                <Input
                    key={controlName + index}
                    type={control.type}
                    name={controlName}
                    placeholder={control.placeholder}
                    value={control.value}
                    label={control.label}
                    onChange={(event) => onChangeHandler(event, controlName)}
                />
            )
        })
    }

    useEffect(() => {
        if (window.M) {
            window.M.Tabs.init(tabsRef.current)
        }
    })

    return (
        <div className="Auth">
            <div className="row">
                <div className="col s6 offset-s3">
                    <ul ref={tabsRef} className="tabs grey darken-4">
                        <li className="tab col s3 offset-s3">
                            <a href="#login">Sign in</a>
                        </li>
                        <li className="tab col s3">
                            <a href="#register">Sign up</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div className="row">
                <div className="col s8 offset-s4">
                    <div id="login" className="col s6">
                        <form onSubmit={submitHandler}>
                            {renderInputs(true)}
                            <button className="btn waves-effect rose-button" onClick={loginHandler}>
                                Sign in
                                <i className="material-icons right">send</i>
                            </button>
                        </form>
                    </div>
                    <div id="register" className="col s6">
                        <form onSubmit={submitHandler}>
                            {renderInputs(false)}
                            <button className="btn waves-effect rose-button" onClick={registerHandler}>
                                Sign up
                                <i className="material-icons right"></i>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}


function mapDispatchToProps(dispatch) {
    return {
        setUserData: (data) => dispatch(setUserData(data)),
    }
}

export default connect(null, mapDispatchToProps)(Auth)
