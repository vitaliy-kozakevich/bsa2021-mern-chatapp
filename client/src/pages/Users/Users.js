import React, { useEffect } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import './Users.scss'
import Button from '../../components/UI/Button/Button'
import { removeUser } from '../../redux/actions/users'

function Users({ users, deleteUser }) {
    useEffect(() => {}, [users])

    console.log(users)
    const renderUsers = () => {
        return users.map(({ email, name, role, _id }) => {
            return (
                <li className="Users__list-item" key={`${_id}` + Math.random()}>
                    <div className="list-item__info">
                        <span>{`Name: ${name}`}</span>
                        <span>{`Email: ${email}`}</span>
                        <span>{`Role: ${role}`}</span>
                    </div>
                    <div className="list-item__btn">
                        <Button clickHandler={() => deleteUser(_id)} name={'Delete'} cls={['error', 'btn']} />
                        <Button clickHandler={() => {}} name={'Edit'} cls={['success', 'btn']} />
                    </div>
                </li>
            )
        })
    }
    return (
        <div className="container">
            <div className="row">
                <div className="col s8 offset-s2">
                    <ul className="Users__list">{renderUsers()}</ul>
                </div>
            </div>
        </div>
    )
}

function mapStateToProps(state) {
    return {
        users: state.users.users,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        deleteUser: (id) => dispatch(removeUser(id)),
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Users))