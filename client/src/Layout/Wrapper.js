import React from 'react'
import './Wrapper.scss'

const Wrapper = ({ children }) => <div className='Wrapper'>{ children }</div>

export default Wrapper