import { createContext } from 'react'

const noop = () => {}

export const ChatContext = createContext({
    inputValue: '',
    inputValueHandler: noop,
    sendMessageHandler: noop,
})