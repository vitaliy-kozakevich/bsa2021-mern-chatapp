import React, { useState } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { removeMessage } from '../../redux/actions/messages'
import Button from '../UI/Button/Button'
// import Modal from '../UI/Modal/Modal'
import './Message.scss'

function Message({
    id,
    avatar,
    text,
    user: user_name,
    createdAt: date,
    userId,
    activeUserId,
    deleteMessageHandler,
    history,
}) {
    const parsedDate = new Date(date).toDateString()
    const isActiveUser = userId === activeUserId

    const openEditMessage = () => {
        history.push('/chat/' + id )
    }

    return (
        <div className={isActiveUser ? 'Message flex-end column' : 'Message'}>
            <div className={isActiveUser ? 'Message__wrap width-50' : 'Message__wrap'}>
                <div className="Message__info primary">
                    <span className="Message__info-name">{user_name}</span>
                    {!isActiveUser && avatar ? (
                        <img className="Message__info-avatar" src={avatar} alt="user_avatar" />
                    ) : null}
                </div>
                <div className="Message__info secondary">
                    <span className="Message__info-text">{text}</span>
                    <span className="Message__info-date">{parsedDate}</span>
                </div>
            </div>
            {!isActiveUser ? (
                <div style={{ minWidth: '20px' }}>
                    {/* <img src={'../../assets/like.svg'} style={{ backgroundColor: 'lightblue' }}/> */}
                </div>
            ) : null}
            {isActiveUser ? (
                <div className="Message__btns">
                    <div className="Message__btns__btn">
                        <Button clickHandler={() => deleteMessageHandler(id)} name={'Delete'} cls={['error']} />
                    </div>
                    <div className="Message__btns__btn">
                        <Button clickHandler={openEditMessage} name={'Edit'} cls={['warn']} />
                    </div>
                </div>
            ) : null}
        </div>
    )
}

function mapStateToProps(state) {
    return {
        activeUserId: state.auth.userId,
    }
}


function mapDispatchToProps(dispatch) {
    return {
        deleteMessageHandler: (id) => dispatch(removeMessage(id)),
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Message))
