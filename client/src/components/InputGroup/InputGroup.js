import React, { useContext } from 'react'
import { ChatContext } from '../context/Chat.context'
import Button from '../UI/Button/Button'
import Input from '../UI/Input/Input'
import './InputGroup.scss'

export default function InputGroup() {
    const { inputValue, inputValueHandler, sendMessageHandler } = useContext(ChatContext)
    return (
        <div className="InputGroup">
            <div className="InputGroup__input">
                <Input value={inputValue} placeholder={'Type message'} onChange={inputValueHandler} />
            </div>
            <div className="InputGroup__btn">
                <Button clickHandler={sendMessageHandler} name={'Send'} cls={['success', 'h-50']} />
            </div>
        </div>
    )
}
