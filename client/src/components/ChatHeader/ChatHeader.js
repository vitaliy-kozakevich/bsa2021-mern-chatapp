import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { 
    setUsersCount, 
    setMessagesCount, 
    setLastMessageDate 
} from '../../redux/actions/chat'
import './ChatHeader.scss'

function Header({
    messages,
    messagesCount,
    usersCount,
    lastMessageDate,
    setUsersCount,
    setMessagesCount,
    setLastMessageDate,
}) {
    useEffect(() => {
        const users = []
        messages.forEach(({ userId }) => {
            if (!users.includes(userId)) users.push(userId)
        })
        setUsersCount(users.length)
    }, [messages])

    useEffect(() => {
        const length = messages.length
        if (length) {
            const lastMessageDate = messages[length - 1].createdAt
            setMessagesCount(length)
            setLastMessageDate(new Date(lastMessageDate).toDateString())
        }
    }, [messages])

    return (
        <div className="ChatHeader">
            <div className="ChatHeader_primary">
                <span>Best chat ever</span>
                <span>{messagesCount > 1 ? `${messagesCount} messages` : `${messagesCount} message`}</span>
                <span>{usersCount > 1 ? `${usersCount} users` : `${usersCount} user`}</span>
            </div>
            <div className="ChatHeader_secondary">Last message at {lastMessageDate}</div>
        </div>
    )
}

function mapStateToProps(state) {
    return {
        messages: state.messages.messages,
        messagesCount: state.chat.messagesCount,
        usersCount: state.chat.usersCount,
        lastMessageDate: state.chat.lastMessageDate,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        setUsersCount: (data) => dispatch(setUsersCount(data)),
        setMessagesCount: (data) => dispatch(setMessagesCount(data)),
        setLastMessageDate: (data) => dispatch(setLastMessageDate(data)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header)
