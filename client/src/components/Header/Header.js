import React from 'react'
import { NavLink } from 'react-router-dom'
import './Header.scss'

const linksAdmin = [
    { to: '/chat', label: 'Chat', exact: false },
    { to: '/users', label: 'Users', exact: false },
]

const linksUser = [{ to: '/chat', label: 'Chat', exact: false }]

const linksAuthFalse = [{ to: '/auth', label: 'Sign in | Sign up', exact: false }]

export default function Header({ isAuthenticated, role }) {
    const rolesLinks = role === 'user' ? linksUser : linksAdmin
    const links = isAuthenticated ? rolesLinks : linksAuthFalse
    const rendeLinks = () => {
        return links.map(({ to, label, exact }, index) => {
            return (
                <li key={index} className={'Link__wrapper'}>
                    <NavLink to={to} exact={exact} activeClassName={'Link__element active'} className={'Link__element'}>
                        {label}
                    </NavLink>
                </li>
            )
        })
    }

    return (
        <nav>
            <div className="nav-wrapper grey darken-4">
                <div className="row">
                    <div className="col s10 offset-s1">
                        <NavLink to="/" exact={true} className="brand-logo logo">
                            Logo
                        </NavLink>
                        <ul id="nav-mobile" className="right hide-on-med-and-down">
                            {rendeLinks()}
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    )
}
