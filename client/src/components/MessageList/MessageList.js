import React, { Fragment, useEffect } from 'react'
import { connect } from 'react-redux'
import Message from '../Message/Message'
import './MessageList.scss'

function MessageList({ messages, user_id }) {
    useEffect(() => {}, [messages])
    return (
        <div className="MessageList">
            {messages.map((m, index) => {
                const { avatar, text, id, user, createdAt, userId } = m
                let isNewDateForMessage
                if (index) {
                    isNewDateForMessage =
                        new Date(messages[index - 1].createdAt).getFullYear() > new Date(createdAt).getFullYear() ||
                        new Date(messages[index - 1].createdAt).getMonth() > new Date(createdAt).getMonth() ||
                        new Date(messages[index - 1].createdAt).getDay() > new Date(createdAt).getDay()
                }

                if (isNewDateForMessage) {
                    return (
                        <Fragment key={id}>
                            <div className="d-flex flex-end f-style">
                                <span>{new Date(createdAt).toDateString()}</span>
                            </div>
                            <hr />
                            <Message {...{ id, avatar, text, user, createdAt, userId }} key={id + Math.random()} />
                        </Fragment>
                    )
                }
                return <Message {...{ id, avatar, text, user, createdAt, userId }} key={id + Math.random()} />
            })}
        </div>
    )
}

function mapStateToProps(state) {
    return {
        messages: state.messages.messages,
        user_id: state.auth.userId,
    }
}

export default connect(mapStateToProps, null)(MessageList)
