import React from 'react'
import './Input.scss'

export default function Input({ placeholder, onChange, value, cls, type, name, label }) {
    const inputType = type || 'text'
    const css = ['Input']
    const htmlFor = `${type}-${Math.random()}`

    if (cls) css.push(cls)

    return (
        <div className="input-field">
            <input
                placeholder={placeholder}
                id={htmlFor}
                type={inputType}
                name={name}
                value={value}
                className={css.join(' ')}
                onChange={onChange}
            />  
            { label ? <label htmlFor={htmlFor}>{label}</label> : null}
        </div>
    )
}
