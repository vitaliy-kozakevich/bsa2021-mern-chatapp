import React from 'react'
import './Button.scss'

export default function Button({ clickHandler, name, cls }) {
    const css = ['Button']
    if (cls) css.push(...cls)
    return (
        <button 
            className={css.join(' ')}
            onClick={clickHandler}
        >
            { name }
        </button>
    )
}
