import React, { useState } from 'react'
import { connect } from 'react-redux'
import {
    editMessage
} from '../../../redux/actions/messages'
import { useMessage } from '../../hooks/message.hook'
import Button from '../Button/Button'
import Input from '../Input/Input'
import './Modal.scss'

function Modal({ 
    messageId,
    text, 
    placeholder,
    editMessageHandler,
    setModalActive
}) {
    const [inputValue, setInputValue] = useState(text)
    const message = useMessage()

    const changeInputValueHandler = ({ target: { value } }) => {
        setInputValue(value)
    }

    return (
        <div className="Modal">
            <div className="row opacity-1">
                <div className="col s12 m6 w-100 opacity-1">
                    <div className="card">
                        <div className="card-content black-text">
                            <span className="card-title">Edit message</span>
                            <Input placeholder={placeholder} value={inputValue} onChange={changeInputValueHandler} />
                        </div>
                        <div className="card-action">
                            <div className="Modal__btns">
                                <div className="Modal__btns__btn">
                                    <Button clickHandler={() => setModalActive(false)} name={'Close'} cls={'warn'} />
                                </div>
                                <div className="Modal__btns__btn">
                                    <Button
                                        clickHandler={() => {
                                            if (!inputValue) {
                                                message('Type your message first')
                                                return
                                            }
                                            editMessageHandler(messageId, inputValue)
                                            setModalActive(false)
                                        }}
                                        name={'Edit'}
                                        cls={'success'}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

function mapDispatchToProps(dispatch) {
    return {
        editMessageHandler:(id, text) => dispatch(editMessage(id, text))
    }
}

export default connect(null, mapDispatchToProps)(Modal)
