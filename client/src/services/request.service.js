export default class RequestService {
    constructor() {
        this.baseUrl = '/api'
    }

    async login(url, method, body) {
        return await this.request(url, method, body)
    }
    async register(url, method, body) {
        return await this.request(url, method, body)
    }
    async request(url, method = 'GET', body = null, headers = {}) {
        try {
            if (body) {
                body = JSON.stringify(body)
                headers['Content-Type'] = 'application/json'
            }

            const response = await fetch(this.baseUrl + url, { method, body, headers })
            const data = await response.json()

            if (!response.ok) {
                throw new Error(data.message || 'Something went wrong!')
            }

            return data
        } catch (error) {
            throw error
        }
    }
}
