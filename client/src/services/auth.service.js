export const storageName = 'userData'

export default class AuthService {
    login(data) {
        localStorage.setItem(
            storageName,
            JSON.stringify({
                ...data,
            })
        )
    }

    logout() {
        localStorage.removeItem(storageName)
    }
}
