import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { BrowserRouter as Router } from 'react-router-dom'
import { routes } from './routes'
import { setUserData } from './redux/actions/auth'
import { fetchMessages } from './redux/actions/messages'
import { fetchUsers } from './redux/actions/users'
import { storageName } from './services/auth.service'
import  Wrapper from './Layout/Wrapper'
import Header from './components/Header/Header'

function App({ token, role, setUserData, fetchMessages, fetchUsers }) {
    useEffect(() => {
        const data = JSON.parse(localStorage.getItem(storageName))
        if (data && data.token) {
            const { name, role, userId } = data
            setUserData({ userId, name, role, token: data.token })
        }
    })

    useEffect(() => {
        if (token) {
            fetchUsers()
            fetchMessages()
        }
    }, [token])

    const isAuthenticated = !!token
    const validatedRoutes = routes(isAuthenticated, role)

    return (
        <Router>
            <Wrapper>
                <Header isAuthenticated={isAuthenticated} role={role} />
                {validatedRoutes}
            </Wrapper>
        </Router>
    )
}


function mapStateToProps(state) {
    return {
        token: state.auth.token,
        role: state.auth.role,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        setUserData: (data) => dispatch(setUserData(data)),
        fetchMessages: () => dispatch(fetchMessages()),
        fetchUsers: () => dispatch(fetchUsers()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)

