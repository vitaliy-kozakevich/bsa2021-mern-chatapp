import React from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'
import Auth from './pages/Auth/Auth'
import Chat from './pages/Chat/Chat'
import EditMessage from './pages/EditMessage/EditMessage'
import EditUser from './pages/EditUser/EditUser'
import Users from './pages/Users/Users'
import Welcome from './pages/Welcome/Welcome'

export const routes = (isAuthenticated, role) => {
    if (isAuthenticated && role === 'admin') {
        return (
            <Switch>
                <Route path="/users" component={Users} />
                <Route exact path="/" component={Welcome} />
                <Route path="/chat/:id" component={EditMessage} />
                <Route path="/chat" component={Chat} />
                <Route path="/users/:id" component={EditUser} />
                <Redirect to="/" />
            </Switch>
        )
    } else if (isAuthenticated && role === 'user') {
        return (
            <Switch>
                <Route path="/chat/:id" component={EditMessage} />
                <Route path="/chat" component={Chat} />
                <Route exact path="/" component={Welcome} />
                <Redirect to="/" />
            </Switch>
        )
    }
    return (
        <Switch>
            <Route exact path="/" component={Welcome} />
            <Route path="/auth" component={Auth} />
            <Redirect to="/" />
        </Switch>
    )
}
