import { 
    FETCH_MESSAGES, 
    ADD_MESSAGE,
    EDIT_MESSAGE,
    DELETE_MESSAGE
} from './actionTypes';
import {
    setLoading,
    setError
} from './chat'
import RequestService from '../../services/request.service'

const appRequest = new RequestService()

function setMessages(data) {
    return {
        type: FETCH_MESSAGES,
        payload: data
    }
}

function addNewMessage(data) {
    return {
        type: ADD_MESSAGE,
        payload: data
    }
}

export function deleteMessage(id) {
    return {
        type: DELETE_MESSAGE,
        payload: id
    }
}

export function editMessage(id, text) {
    return {
        type: EDIT_MESSAGE,
        payload: {
            id,
            text
        }
    }
}

export function fetchMessages() {
    return async dispatch => {
        dispatch(setLoading(true))
        try {
            const response = await appRequest.request('/messages')

            dispatch(setLoading(false))
            dispatch(setMessages(response.data))
        } catch (error) {
            dispatch(setLoading(false))
            dispatch(setError(error.message))
        } 
    }
}

export function addMessage(text, user) {
    return async (dispatch) => {
        try {
            const response = await appRequest.request('/messages', 'POST', {
                ...user,
                text,
            })
            dispatch(addNewMessage(response.data))
        } catch (error) {
            dispatch(setError(error.message))
        }
    }
}

export function removeMessage(id) {
    return async (dispatch) => {
        try {
            const response = await appRequest.request(`/messages/${id}`, 'DELETE')
            dispatch(deleteMessage(response.id))
        } catch (error) {
            dispatch(setError(error.message))
        }
    }
}

export function editMessageText(messageId, newText) {
    return async (dispatch) => {
        try {
            const { id, text } = await appRequest.request(`/messages/${messageId}`, 'PUT', {
                text: newText,
            })
            dispatch(editMessage(id, text))
        } catch (error) {
            dispatch(setError(error.message))
        }
    }
}