import { FETCH_USERS, ADD_USER, EDIT_USER, DELETE_USER } from './actionTypes'
import { setLoading, setError } from './chat'
import RequestService from '../../services/request.service'

const appRequest = new RequestService()

function getUsers(data) {
    return {
        type: FETCH_USERS,
        payload: data,
    }
}

function addNewUser(data) {
    return {
        type: ADD_USER,
        payload: data,
    }
}

export function deleteUser(id) {
    return {
        type: DELETE_USER,
        payload: id,
    }
}

export function updateUser(id, user) {
    return {
        type: EDIT_USER,
        payload: {
            id,
            user,
        },
    }
}

export function fetchUsers() {
    return async (dispatch) => {
        dispatch(setLoading(true))
        try {
            const { users } = await appRequest.request('/users')

            dispatch(setLoading(false))
            dispatch(getUsers(users))
        } catch (error) {
            dispatch(setLoading(false))
            dispatch(setError(error.message))
        }
    }
}

export function addUser(user) {
    return async (dispatch) => {
        dispatch(setLoading(true))
        try {
            const response = await appRequest.request('/users', 'POST', {
                ...user,
            })
            dispatch(setLoading(false))
            dispatch(addNewUser(response.data))
        } catch (error) {
            dispatch(setLoading(false))
            dispatch(setError(error.message))
        }
    }
}

export function removeUser(id) {
    return async (dispatch) => {
        try {
            const response = await appRequest.request(`/users/${id}`, 'DELETE')
            dispatch(deleteUser(response.id))
        } catch (error) {
            dispatch(setError(error.message))
        }
    }
}

export function editUser(userId, userData) {
    return async (dispatch) => {
        try {
            const { id, user } = await appRequest.request(`/users/${userId}`, 'PUT', {
                ...userData,
            })
            dispatch(updateUser(id, user))
        } catch (error) {
            dispatch(setError(error.message))
        }
    }
}
