import { 
    SET_USERS_COUNT, 
    SET_MESSAGES_COUNT, 
    SET_LAST_MESSAGE_DATE,
    SET_LOADING,
    SET_ERROR
 } from './actionTypes';

export function setUsersCount(data) {
    return {
        type: SET_USERS_COUNT,
        payload: data
    }
}

export function setMessagesCount(data) {
    return {
        type: SET_MESSAGES_COUNT,
        payload: data
    }
}

export function setLastMessageDate(data) {
    return {
        type: SET_LAST_MESSAGE_DATE,
        payload: data
    }
}

export function setLoading(data) {
    return {
        type: SET_LOADING,
        payload: data
    }
}

export function setError(data) {
    return {
        type: SET_ERROR,
        payload: data
    }
}