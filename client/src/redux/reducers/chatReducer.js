import {
    SET_LOADING,
    SET_ERROR,
    SET_USERS_COUNT,
    SET_MESSAGES_COUNT,
    SET_LAST_MESSAGE_DATE,
} from '../actions/actionTypes'

const initialState = {
    loading: false,
    error: '',
    usersCount: 0,
    messagesCount: 0,
    lastMessageDate: '',
}

export default function chat(state = initialState, action) {
    switch(action.type) {
        case SET_USERS_COUNT:
            return {
                ...state,
                usersCount: action.payload
                
            }
        case SET_MESSAGES_COUNT:
            return {
                ...state,
                messagesCount: action.payload
            }
        case SET_LAST_MESSAGE_DATE: 
            return {
                ...state,
                lastMessageDate: action.payload
            }
        case SET_LOADING:
            return {
                ...state,
                loading: action.payload
            }
        case SET_ERROR: 
            return {
                ...state,
                error: action.payload
            }  
        default:
            return state
    }
}