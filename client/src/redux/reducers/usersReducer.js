import { ADD_USER, DELETE_USER, EDIT_USER, FETCH_USERS } from '../actions/actionTypes'

const initialState = {
    users: [],
}

export default function users(state = initialState, action) {
    switch (action.type) {
        case FETCH_USERS:
            return {
                ...state,
                users: [...state.users, ...action.payload],
            }
        case ADD_USER:
            return {
                ...state,
                users: [...state.users, action.payload],
            }
        case DELETE_USER:
            console.log(state.users, action.payload)
            const updatedUsers = state.users.filter((u) => u._id !== action.payload)
            return {
                ...state,
                users: updatedUsers,
            }
        case EDIT_USER:
            return {
                ...state,
            }
        default:
            return state
    }
}
