import { SET_USER_DATA } from '../actions/actionTypes'

const initialState = {
    userId: null,
    name: null,
    role: null,
    token: null,
}

export default function auth(state = initialState, action) {
  switch (action.type) {
      case SET_USER_DATA:
          return {
              ...state,
              ...action.payload,
          }
      default:
          return state
  }
}