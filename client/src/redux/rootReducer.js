import { combineReducers } from 'redux'

import messages from './reducers/messagesReducer'
import users from './reducers/usersReducer'
import chat from './reducers/chatReducer'
import auth from './reducers/authReducer'


export default combineReducers({
    messages,
    chat,
    auth,
    users,
})